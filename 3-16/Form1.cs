﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3_16
{
    public partial class Form1 : Form
    {
        const int iTimerInterval = 25;
        const int iBallSize = 12;
        const int iMoveSize = 4;

        Bitmap bitmap;

        int xCenter, yCenter;
        int cxRad, cyRad;
        int cxMove, cyMove;
        int cxTotal, cyTotal;

        public Form1()
        {
            InitializeComponent();

            this.BackColor = Color.White;

            Timer timer = new Timer();
            timer.Interval = iTimerInterval;
            timer.Tick += new EventHandler(TimerOnTick);
            timer.Start();

            this.ResizeRedraw = true;

            xCenter = ClientSize.Width / 2;
            yCenter = ClientSize.Height / 2;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            SolidBrush brush = new SolidBrush(Color.Orange);
            Graphics g = e.Graphics;
            g.Clear(BackColor);

            cxRad = ClientSize.Width / iBallSize;
            cyRad = ClientSize.Height / iBallSize;

            cxMove = Math.Max(1, cxRad / iMoveSize);
            cyMove = Math.Max(1, cyRad / iMoveSize);

            cxTotal = 2 * (cxRad + cxMove);
            cyTotal = 2 * (cyRad + cyMove);

            bitmap = new Bitmap(cxRad + cxMove*2, cyRad + cxMove*2);

            g = Graphics.FromImage(bitmap);

            g.Clear(BackColor);

            g.FillEllipse(brush, new Rectangle(cxMove, cyMove, cxRad, cyRad));

            e.Dispose();
            brush.Dispose();


        }
        void TimerOnTick(object obj, EventArgs ea)
        {
            Graphics g = CreateGraphics();
            g.DrawImage(bitmap, xCenter - (cxTotal / 2), yCenter - (cyTotal / 2), cxTotal, cyTotal);

            xCenter += cxMove;
            yCenter += cyMove;

            if (xCenter + cxRad >= DisplayRectangle.Width || xCenter - cxRad <= 0)
                cxMove = -cxMove;
            if (yCenter + cyRad >= DisplayRectangle.Height || yCenter - cyRad <= 0)
                cyMove = -cyMove;
        }
    }
}
